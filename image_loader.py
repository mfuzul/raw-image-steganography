import numpy as np
import rawpy


def rel_image_path(file_name: str) -> str:
    return 'img/' + file_name


def load_raw_img_to_array(file_name: str) -> rawpy._rawpy.RawPy:
    return rawpy.imread(rel_image_path(file_name))
