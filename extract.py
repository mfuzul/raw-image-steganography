from PIL import Image
import numpy as np

replace_bits = 6

stego_image = Image.open('stego_photo_{0}_bits.jpg'.format(replace_bits))

width, height = stego_image.size

recover_image_msb_4 = Image.new('RGB', stego_image.size)
recover_image_lsb_4 = Image.new('RGB', stego_image.size)
# recover_image_middle_4 = Image.new('RGB', stego_image.size)

np_stego = np.array(stego_image)


def lsb_4(r: int, g: int, b: int, recover_image):
    r_r = int("{0:08b}".format(r)[4:] + ("0" * 4), 2)
    r_g = int("{0:08b}".format(g)[4:] + ("0" * 4), 2)
    r_b = int("{0:08b}".format(b)[4:] + ("0" * 4), 2)

    recover_image.putpixel((x, y), (r_r, r_g, r_b))

    print("Stego: r({0:08b}), g({1:08b}), b({2:08b})".format(r_r, r_g, r_b))


def msb_4(r: int, g: int, b: int, recover_image):
    r_r = int("{0:08b}".format(r)[:4] + ("0" * 4), 2)
    r_g = int("{0:08b}".format(g)[:4] + ("0" * 4), 2)
    r_b = int("{0:08b}".format(b)[:4] + ("0" * 4), 2)

    recover_image.putpixel((x, y), (r_r, r_g, r_b))

    print("Stego: r({0:08b}), g({1:08b}), b({2:08b})".format(r_r, r_g, r_b))


for x in range(width):
    for y in range(height):
        r, g, b = stego_image.getpixel((x, y))

        print("Original: r({0:08b}), g({1:08b}), b({2:08b})".format(r, g, b))

        lsb_4(r, g, b, recover_image_lsb_4)
        msb_4(r, g, b, recover_image_msb_4)


recover_image_msb_4.save("recover_photo_msb_4_{0}_bits.jpg".format(replace_bits))
recover_image_lsb_4.save("recover_photo_lsb_4_{0}_bits.jpg".format(replace_bits))
# recover_image_middle_4.save("recover_photo_middle_4_{0}_bits.jpg".format(replace_bits))
