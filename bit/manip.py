

# send x as binary
# return a binary
def msb(x: bin) -> bin:
    temp_x = str(x)

    print(temp_x)

    return bin(int(temp_x[0]))


# take y's MSB and place it on x's LSB
def switch_msb_to_lsb(x: bin, y: bin) -> bin:
    print(len(x))
    print(len(y))

    return x | msb(y)


def prepend_zeros(x: bin) -> str:
    diff = 16 - len(x[2:])
    if diff > 0:
        return diff * '0' + str(x)[2:]

    return str(x)[2:]


# numbers are expected to be 16 bits
# 5 bits for red, 6 bits for green and 5 bits for blue
def lsb(x: bin, y: bin, replace_bits: int = 4) -> bin:
    x_temp = x[2:]
    y_temp = y[2:]

    return bin(int(x_temp[:len(x_temp)-replace_bits] + y_temp[:replace_bits], 2))

if '__main__' == __name__ :
    a = bin(2029)
    b = bin(3039)

    print("a = {0}".format(a))
    print("b = {0}".format(b))

    print(lsb(a, b))
