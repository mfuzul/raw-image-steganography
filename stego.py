import matplotlib.pyplot as plt
from image_loader import load_raw_img_to_array
import numpy as np

cover_photo = load_raw_img_to_array('cover_photo.CR2')
hidden_photo = load_raw_img_to_array('hidden_photo.CR2')

processed_cover_photo = cover_photo.postprocess(
    use_camera_wb=True
)
processed_hidden_photo = hidden_photo.postprocess(
    use_camera_wb=True
)

vect_proc_cover_photo = np.matrix.flatten(processed_cover_photo)
vect_proc_hidden_photo = np.matrix.flatten(processed_hidden_photo)


def foo(x, y):
    temp_x = bin(x)[2:]
    if len(temp_x) < 8:
        temp_x = '0' * (8 - len(temp_x)) + temp_x

    temp_y = bin(y)[2:]
    if len(temp_y) < 8:
        temp_y = '0' * (8 - len(temp_y)) + temp_y

    return int(
        temp_x[:4] +
        temp_y[:4],
        2
    )


vect_proc_stego_photo = [
    foo(vect_proc_cover_photo[i], vect_proc_hidden_photo[i]) for i in range(0, len(vect_proc_cover_photo))
]

processed_stego_photo = np.reshape(vect_proc_stego_photo, processed_cover_photo.shape)

print(processed_stego_photo)

plt.imshow(processed_stego_photo)
plt.show()

# plt.imshow(processed_cover_photo)
# plt.show()
# plt.imshow(processed_hidden_photo)
# plt.show()

cover_photo.close()
hidden_photo.close()
