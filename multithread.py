import logging
import threading
import time
import concurrent.futures

totalSum = 0


def total(array: list) -> int:
    global totalSum

    for num in array:
        totalSum += num

    return totalSum


if __name__ == '__main__':
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")

    x = range(1, 11)
    x_len = len(x)
    divide = 4
    chunk_size = x_len // divide

    threads = list()

    start_time = time.time()
    for i in range(divide+1):
        logging.info("Main    : create and start thread %d.", i)
        t = threading.Thread(target=total, args=(x[chunk_size*i:chunk_size*(i+1)],))
        threads.append(t)
        t.start()

    for i, t in enumerate(threads):
        logging.info("Main    : before joining thread %d.", i)
        t.join()
        logging.info("Main    : thread %d done", i)

    duration = time.time() - start_time
    print(f'Total = {totalSum}, finished in {duration} seconds')

# def thread_function(name):
#     logging.info("Thread %s: starting", name)
#     time.sleep(2)
#     logging.info("Thread %s: finishing", name)
#
# if __name__ == "__main__":
#     format = "%(asctime)s: %(message)s"
#     logging.basicConfig(format=format, level=logging.INFO,
#                         datefmt="%H:%M:%S")
#
#     threads = list()
#     for index in range(3):
#         logging.info("Main    : create and start thread %d.", index)
#         x = threading.Thread(target=thread_function, args=(index,))
#         threads.append(x)
#         x.start()
#
#     for index, thread in enumerate(threads):
#         logging.info("Main    : before joining thread %d.", index)
#         thread.join()
#         logging.info("Main    : thread %d done", index)